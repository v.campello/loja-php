<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

class Pedidos extends ActiveRecord
{
    public static function tableName()
    {
        return 'pedidos';
    }

    public function rules()
    {
        return [
            [['produto'], 'string'],
            [['produto', 'data', 'valor', 'clienteId', 'pedidoStatusId', 'ativo'], 'required', 'message' => 'Campo preciso'],
            [['data'], 'string'],
            [['valor'], 'string'],
            [['clienteId', 'pedidoStatusId'], 'integer'],
            [['ativo'], 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'produto' => 'Nome do Produto',
            'valor' => 'Valor',
            'data' => 'Data do Pedido',
            'clienteId' => 'Cliente',
            'pedidoStatusId' => 'Situação do Pedido',
        ];
    }

    public function getCliente()
    {
        return $this->hasOne(Clientes::class, ['id' => 'clienteId']);
    }

    public function getPedidoStatus()
    {
        return $this->hasOne(PedidoStatus::class, ['id' => 'pedidoStatusId']);
    }

    public function retrieveAll()
    {
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_ASC]
            ]
            ]);

            return $dataProvider;
    }
}
