<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

class PedidoStatus extends ActiveRecord
{
    public static function tableName()
    {
        return 'pedido_status';
    }

    public function rules()
    {
        return [
            [['descricao'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'descricao' => 'Descrição do Produto'
        ];
    }


}
