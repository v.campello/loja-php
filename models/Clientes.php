<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

class Clientes extends ActiveRecord
{
    public static function tableName()
    {
        return 'clientes';
    }

    public function rules()
    {
        return [
            [['nome', 'cpf', 'telefone', 'data_nasc'], 'string' ],
            [['cpf'], function ($attribute, $params, $validator) {
                if (!$this->validCpf()) {
                    $this->addError($attribute, 'Cpf inválido');
                }
            }],
            [['nome', 'cpf', 'telefone', 'data_nasc'], 'required', 'message' => 'Campo obrigatório' ],
            [['ativo'], 'boolean' ],
        ];
    }

    public function validCpf()
    {
        $cpf = $this->cpf;
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }

        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
    return true;
    }

    public function attributeLabels()
    {
        return [
            'nome' => 'Nome do Cliente',
            'cpf' => 'CPF',
            'telefone' => 'Telefone de Contato',
            'data_nasc' => 'Data de Nascimento',
            'ativo' => 'Status do Cliente',
        ];
    }

    public function retrieveAll($formData)
    {
        // echo "<pre>";
        // var_dump(); die;
        $query = self::find();
        if(isset($formData['Clientes']['nome']) && !empty($formData['Clientes']['nome'])){
            $query->where(['nome' => $formData['Clientes']['nome']]);
        }
        $dataProvider = new ActiveDataProvider ([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_ASC]
            ]
        ]);

        return $dataProvider;
    }

    public function listClientes(){

        $query = self::find();
        $clientes = $query->all();
        $list = [];
        foreach($clientes as $cliente){
            $list[$cliente->id] = $cliente->nome;
        }

        // echo "<pre>";
        // var_dump($list); die;

        return $list;
    }
}
