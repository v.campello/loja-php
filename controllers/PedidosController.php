<?php

namespace app\controllers;

use app\models\Clientes;
use app\models\Pedidos;
use Yii;
use yii\web\Controller;

class PedidosController extends Controller
{
    public $modelPedidos;
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->modelPedidos = new Pedidos();
    }
    public function actionIndex()
    {
        $model = new Pedidos();
        $pedidos = $model->find()->where([])->all();

        return $this->render('index', ['pedidos' => $pedidos, 'modelPedidos' => $model]);
    }

     public function actionCreate()
    {
        if (Yii::$app->request->getIsPost()){
            $formData = Yii::$app->request->post();
            $this->modelPedidos->load($formData);
            $this->modelPedidos->ativo = true;
            if($this->modelPedidos->save()){
                return $this->redirect('/pedidos');
            }
        }
        $modelClientes = new Clientes();
        $clientes = $modelClientes->listClientes();
        // echo "<pre>";
        // var_dump($clientes); die;

        return $this->render('create', [
            'pedidos' => $this->modelPedidos,
            'pessoas' => $clientes,
        ]);
    }

     public function actionUpdate($id)
    {
        $pedido = $this->modelPedidos->findOne($id);
        if (Yii::$app->request->getIsPost()){
            $formData = Yii::$app->request->post();
            $pedido->load($formData);
            if($pedido->save()){
                return $this->redirect('/pedidos');
            }
        }

        return $this->render('update', ['pedidos' => $pedido]);
    }

    public function actionView($id)
    {
        $pedidos = $this->modelPedidos->findOne($id);

        return $this->render('view', ['pedidos' => $pedidos]);
    }

    public function actionDelete($id)
    {
        $pedido = $this->modelPedidos->findOne($id);
        $pedido->ativo=0;
        $pedido->save();

        return $this->redirect('/pedidos');
    }
}
