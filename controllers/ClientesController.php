<?php

namespace app\controllers;

use app\models\Clientes;
use Yii;
use yii\web\Controller;

class ClientesController extends Controller
{
    public $modelCliente;
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->modelCliente = new Clientes();
    }

    public function actionIndex()
    {
        $model = new Clientes();
        $formData = [];
        if (Yii::$app->request->getIsPost()){
            $formData = Yii::$app->request->post();
        }

        $dataProvider = $model->retrieveAll($formData);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'cliente' => $this->modelCliente
        ]);
    }

    public function actionCreate()
    {
        if (Yii::$app->request->getIsPost()){
            $formData = Yii::$app->request->post();
            $this->modelCliente->load($formData);
            $this->modelCliente->ativo = true;
            if($this->modelCliente->save()){
                return $this->redirect('/clientes');
            }
        }

        return $this->render('create', [
            'cliente' => $this->modelCliente
        ]);
    }

    public function actionUpdate($id)
    {
        $modelCliente = new Clientes();
        $cliente = $modelCliente->findOne($id);
        if (Yii::$app->request->getIsPost()){
            $formData = Yii::$app->request->post();
            $cliente->load($formData);
            if($cliente->save()){
                return $this->redirect('/clientes');
            }
        }

        return $this->render('update', ['cliente' => $cliente]);
    }

    public function actionView($id)
    {
        $cliente = $this->modelCliente->findOne($id);

        return $this->render('view', ['cliente' => $cliente]);
    }

    public function actionDelete($id)
    {
        $cliente = $this->modelCliente->findOne($id);
        $cliente->ativo=0;
        if($cliente->save()){
            Yii::$app->session->setFlash('success', 'Cliente atualizado com sucesso');
        } else {
            Yii::$app->session->setFlash('danger', 'Cliente não pode ser atualizado');
        }



        // Clientes::updateAll([
        //     ['ativo' => false],
        //     ['id' => $id] /**verificar como finalizar esse update */
        // ]);

        // var_dump($cliente->errors);
        // die;

        return $this->redirect('/clientes');
    }

}
