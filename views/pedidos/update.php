<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$form = ActiveForm::begin();

echo $form->field($pedidos, 'produto') ;
echo $form->field($pedidos, 'valor') ;
echo $form->field($pedidos, 'data') ;
echo $form->field($pedidos, 'clienteId') ;
echo $form->field($pedidos, 'pedidoStatusId') ;

echo Html::submitButton('Salvar');

ActiveForm::end();
