<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$form = ActiveForm::begin();

echo $form->field($pedidos, 'produto') ;
echo $form->field($pedidos, 'valor') ;
echo $form->field($pedidos, 'data') ;
echo $form->field($pedidos, 'clienteId')->dropdownList(
    $pessoas, ['prompt' => 'selecione']
) ;
echo $form->field($pedidos, 'pedidoStatusId') ;

echo Html::submitButton('Salvar', ['class' => 'btn btn-primary']);

echo Html::a('Voltar', '/pedidos', ['class' => 'btn btn-secondary']);


ActiveForm::end();
