<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$form = ActiveForm::begin();


echo $form->field($cliente, 'nome') ;
echo $form->field($cliente, 'cpf') ;
echo $form->field($cliente, 'data_nasc') ;
echo $form->field($cliente, 'telefone') ;

echo Html::submitButton('Salvar', ['class' => 'btn btn-primary']);

echo Html::a('Voltar', '/clientes', ['class' => 'btn btn-secondary']);


ActiveForm::end();
$this->registerJsFile('@web/js/maskDate.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web/js/mask.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web/js/index.js', ['depends' => 'yii\web\YiiAsset']);
