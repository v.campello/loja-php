<?php

use yii\bootstrap4\Html;
use yii\grid\GridView;

?>
<div>
    <h1>Listagem de Clientes</h1>
    <?php
        echo $this->render('_filtros', ['cliente' => $cliente]);
        echo Html::a('Criar Cliente', 'clientes/create', ['class'=> 'btn btn-primary']);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model, $key, $index, $grid) {
                    return [
                        'style' => "cursor: pointer",
                    ];
            },
        'columns' => [
            'id',
            'nome',
            'cpf',
            [
                'attribute' => 'data_nasc',
                'value' => function ($model){
                    return date('d/m/Y', strtotime($model->data_nasc));
                }
            ],
            'telefone',
            [
                'attribute' => 'ativo',
                'value' => function ($model){
                    if($model->ativo == 1){
                        return 'ativo';
                    }
                    return 'inativo';
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],

    ]); ?>
</div>
